.. Smart documentation master file, created by
   sphinx-quickstart on Sun May 31 00:33:28 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Smart's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   vpn_wireguard/wireguard_log

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
