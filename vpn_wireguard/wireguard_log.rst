====================
Wireguard log issues
====================

    :Author: Jared Huang



1 syslog case
-------------

.. table::

    +------------+-----------------------------------------------------------+-------------+
    | Event Code | Meaning                                                   | Category    |
    +============+===========================================================+=============+
    | VPN021     | VPN connection error. (excluding authentication error.)   | Error       |
    +------------+-----------------------------------------------------------+-------------+
    | VPN022     | VPN connection error. (preshared key authentication only) | Error       |
    +------------+-----------------------------------------------------------+-------------+
    | VPN023     | VPN connection setup                                      | Information |
    +------------+-----------------------------------------------------------+-------------+
    | VPN024     | VPN connection released                                   | Information |
    +------------+-----------------------------------------------------------+-------------+
    | VPN025     | VPN connection released because of connection time out.   | Information |
    +------------+-----------------------------------------------------------+-------------+

2 wireguard.conf
----------------

.. code:: example

    [Interface]
    PrivateKey = gNqEY0IPgvPJgGwXK9y8xrF9kZ2qBCx1CZ3rzXw/rXQ=
    Address = 10.200.200.12/24
    DNS = 192.168.2.1

    [Peer]
    PublicKey = V+0zZCay9AaHe8Fgk8Zxx+W0v9VLe8vrNl9tKpUDpGI=
    PresharedKey = 0aJyHqNdJgMyMedFZMDJ2dHksDab3qRKMQf08NNWQKc=
    AllowedIPs = 0.0.0.0/0
    Endpoint = 172.16.7.183:53280
    PersistentKeepalive = 21

3 VPN021 connection error excluding authentication error.
---------------------------------------------------------

3.1 excluding authentication error??
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

3.1.1 error PublicKey, error Address, error PrivateKey
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. table::

    +------------------------+---------------------------------------------------------+------------------+
    | Test                   | WG log shown                                            | continuously log |
    +========================+=========================================================+==================+
    | bad client private key | Invalid handshake initation from IP:Port                | yes              |
    +------------------------+---------------------------------------------------------+------------------+
    | bad client Address     | Package has unallowed src IP (IP) from peer X (IP:Port) | yes              |
    +------------------------+---------------------------------------------------------+------------------+
    | bad server publickey   | Invalid MAC of handshake, dropping packet from IP:Port  | yes              |
    +------------------------+---------------------------------------------------------+------------------+

4 VPN022 connection error (PSK only)
------------------------------------

4.1 PSK error log
~~~~~~~~~~~~~~~~~

.. code:: example

    root@Speedport Smart 4:~# dmesg | grep wireguard
    wireguard: wg0: Peer 1 created
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:53370)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:53370)
    wireguard: wg0: Keypair 1 created for peer 1
    wireguard: wg0: Receiving keepalive packet from peer 1 (172.16.7.172:53370)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.172:53370)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.172:53370)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.172:53370)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.172:53370)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.172:53370)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.172:53370)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.172:53370)
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:57763)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:57763)
    wireguard: wg0: Keypair 2 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:57763)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:57763)
    wireguard: wg0: Keypair 2 destroyed for peer 1
    wireguard: wg0: Keypair 3 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:57763)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:57763)
    wireguard: wg0: Keypair 3 destroyed for peer 1
    wireguard: wg0: Keypair 4 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:57763)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:57763)
    wireguard: wg0: Keypair 4 destroyed for peer 1
    wireguard: wg0: Keypair 5 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:57763)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:57763)
    wireguard: wg0: Keypair 5 destroyed for peer 1
    wireguard: wg0: Keypair 6 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:57763)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:57763)
    wireguard: wg0: Keypair 6 destroyed for peer 1
    wireguard: wg0: Keypair 7 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:57763)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:57763)
    wireguard: wg0: Keypair 7 destroyed for peer 1
    wireguard: wg0: Keypair 8 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:57763)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:57763)
    wireguard: wg0: Keypair 8 destroyed for peer 1
    wireguard: wg0: Keypair 9 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:57763)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:57763)
    wireguard: wg0: Keypair 9 destroyed for peer 1
    wireguard: wg0: Keypair 10 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 10 destroyed for peer 1
    wireguard: wg0: Keypair 11 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 11 destroyed for peer 1
    wireguard: wg0: Keypair 12 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 12 destroyed for peer 1
    wireguard: wg0: Keypair 13 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 13 destroyed for peer 1
    wireguard: wg0: Keypair 14 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 14 destroyed for peer 1
    wireguard: wg0: Keypair 15 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 15 destroyed for peer 1
    wireguard: wg0: Keypair 16 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 16 destroyed for peer 1
    wireguard: wg0: Keypair 17 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 17 destroyed for peer 1
    wireguard: wg0: Keypair 18 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 18 destroyed for peer 1
    wireguard: wg0: Keypair 19 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 19 destroyed for peer 1
    wireguard: wg0: Keypair 20 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 20 destroyed for peer 1
    wireguard: wg0: Keypair 21 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 21 destroyed for peer 1
    wireguard: wg0: Keypair 22 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 22 destroyed for peer 1
    wireguard: wg0: Keypair 23 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 23 destroyed for peer 1
    wireguard: wg0: Keypair 24 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 24 destroyed for peer 1
    wireguard: wg0: Keypair 25 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 25 destroyed for peer 1
    wireguard: wg0: Keypair 26 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 26 destroyed for peer 1
    wireguard: wg0: Keypair 27 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 27 destroyed for peer 1
    wireguard: wg0: Keypair 28 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 28 destroyed for peer 1
    wireguard: wg0: Keypair 29 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 29 destroyed for peer 1
    wireguard: wg0: Keypair 30 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 30 destroyed for peer 1
    wireguard: wg0: Keypair 31 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 31 destroyed for peer 1
    wireguard: wg0: Keypair 32 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 32 destroyed for peer 1
    wireguard: wg0: Keypair 33 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 33 destroyed for peer 1
    wireguard: wg0: Keypair 34 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 34 destroyed for peer 1
    wireguard: wg0: Keypair 35 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 35 destroyed for peer 1
    wireguard: wg0: Keypair 36 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 36 destroyed for peer 1
    wireguard: wg0: Keypair 37 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 37 destroyed for peer 1
    wireguard: wg0: Keypair 38 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 38 destroyed for peer 1
    wireguard: wg0: Keypair 39 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 39 destroyed for peer 1
    wireguard: wg0: Keypair 40 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 40 destroyed for peer 1
    wireguard: wg0: Keypair 41 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 41 destroyed for peer 1
    wireguard: wg0: Keypair 42 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 42 destroyed for peer 1
    wireguard: wg0: Keypair 43 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 43 destroyed for peer 1
    wireguard: wg0: Keypair 44 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 44 destroyed for peer 1
    wireguard: wg0: Keypair 45 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 45 destroyed for peer 1
    wireguard: wg0: Keypair 46 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 46 destroyed for peer 1
    wireguard: wg0: Keypair 47 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 47 destroyed for peer 1
    wireguard: wg0: Keypair 48 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 48 destroyed for peer 1
    wireguard: wg0: Keypair 49 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 49 destroyed for peer 1
    wireguard: wg0: Keypair 50 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 50 destroyed for peer 1
    wireguard: wg0: Keypair 51 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 51 destroyed for peer 1
    wireguard: wg0: Keypair 52 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 52 destroyed for peer 1
    wireguard: wg0: Keypair 53 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 53 destroyed for peer 1
    wireguard: wg0: Keypair 54 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 54 destroyed for peer 1
    wireguard: wg0: Keypair 55 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 55 destroyed for peer 1
    wireguard: wg0: Keypair 56 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 56 destroyed for peer 1
    wireguard: wg0: Keypair 57 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 57 destroyed for peer 1
    wireguard: wg0: Keypair 58 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 58 destroyed for peer 1
    wireguard: wg0: Keypair 59 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 59 destroyed for peer 1
    wireguard: wg0: Keypair 60 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 60 destroyed for peer 1
    wireguard: wg0: Keypair 61 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 61 destroyed for peer 1
    wireguard: wg0: Keypair 62 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 62 destroyed for peer 1
    wireguard: wg0: Keypair 63 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 63 destroyed for peer 1
    wireguard: wg0: Keypair 64 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 64 destroyed for peer 1
    wireguard: wg0: Keypair 65 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 65 destroyed for peer 1
    wireguard: wg0: Keypair 66 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 66 destroyed for peer 1
    wireguard: wg0: Keypair 67 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 67 destroyed for peer 1
    wireguard: wg0: Keypair 68 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 68 destroyed for peer 1
    wireguard: wg0: Keypair 69 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 69 destroyed for peer 1
    wireguard: wg0: Keypair 70 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 70 destroyed for peer 1
    wireguard: wg0: Keypair 71 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 71 destroyed for peer 1
    wireguard: wg0: Keypair 72 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 72 destroyed for peer 1
    wireguard: wg0: Keypair 73 created for peer 1
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.172:54798)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.172:54798)
    wireguard: wg0: Keypair 73 destroyed for peer 1
    wireguard: wg0: Keypair 74 created for peer 1
    wireguard: wg0: Zeroing out all keys for peer 1 (172.16.7.172:54798), since we haven't received a new one in 540 seconds
    wireguard: wg0: Keypair 74 destroyed for peer 1
    wireguard: wg0: Keypair 1 destroyed for peer 1

4.2 PSK error
~~~~~~~~~~~~~

.. table::

    +---------------+---------------------------------+------------------+
    | Test          | WG log shown                    | continuously log |
    +===============+=================================+==================+
    | Error PSK key | Keypair 47 destroyed for peer 1 | yes              |
    +---------------+---------------------------------+------------------+

5 VPN023 connection setup
-------------------------

.. code:: example

    root@Speedport Smart 4:~# dmesg | grep wireguard
    wireguard: wg0: Peer 1 created
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Keypair 1 created for peer 1
    wireguard: wg0: Receiving keepalive packet from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Receiving keepalive packet from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Receiving keepalive packet from peer 1 (172.16.7.190:54352)

6 VPN024 connection released
----------------------------

.. code:: example

    root@Speedport Smart 4:~# dmesg | grep wireguard
    wireguard: wg0: Peer 1 created
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Keypair 1 created for peer 1
    wireguard: wg0: Receiving keepalive packet from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Receiving keepalive packet from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Receiving keepalive packet from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Sending keepalive packet to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Receiving keepalive packet from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Receiving keepalive packet from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Receiving handshake initiation from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Sending handshake response to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Keypair 2 created for peer 1
    wireguard: wg0: Receiving keepalive packet from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Failed to give packet to userspace from peer 1 (172.16.7.190:54352)
    wireguard: wg0: Retrying handshake with peer 1 (172.16.7.190:54352) because we stopped hearing back after 15 seconds
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 2)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 3)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 4)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Retrying handshake with peer 1 (172.16.7.190:54352) because we stopped hearing back after 15 seconds
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 2)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 3)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 4)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 5)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Retrying handshake with peer 1 (172.16.7.190:54352) because we stopped hearing back after 15 seconds
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 2)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 3)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 4)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Retrying handshake with peer 1 (172.16.7.190:54352) because we stopped hearing back after 15 seconds
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 2)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 3)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 4)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 5)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Retrying handshake with peer 1 (172.16.7.190:54352) because we stopped hearing back after 15 seconds
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 2)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 3)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 4)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 5)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Retrying handshake with peer 1 (172.16.7.190:54352) because we stopped hearing back after 15 seconds
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 2)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 3)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 4)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 5)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 6)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 7)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 8)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 9)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Retrying handshake with peer 1 (172.16.7.190:54352) because we stopped hearing back after 15 seconds
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 2)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 3)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 4)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 5)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 6)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 7)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 8)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 9)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 10)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 11)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 12)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 13)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 14)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 15)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 16)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 17)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 18)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 19)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 5 seconds, retrying (try 20)
    wireguard: wg0: Sending handshake initiation to peer 1 (172.16.7.190:54352)
    wireguard: wg0: Handshake for peer 1 (172.16.7.190:54352) did not complete after 20 attempts, giving up
    wireguard: wg0: Zeroing out all keys for peer 1 (172.16.7.190:54352), since we haven't received a new one in 540 seconds
    wireguard: wg0: Keypair 1 destroyed for peer 1
    wireguard: wg0: Keypair 2 destroyed for peer 1

7 VPN025
--------

If user disconnect from WiFi AP or unplug ethernet, WireGuard has the same log as VPN024.
So, WireGuard uses the same algorithm for VPN024 and VPN025.

8 540 seconds
-------------

wg\_queued\_expired\_zero\_key\_material
(): REJECT\_AFTER\_TIME \* 3)
REJECT\_AFTER\_TIME: ./messages.h:46:	REJECT\_AFTER\_TIME = 180,

9 Assumption and Limitation
---------------------------

After analyzing the WireGuard debugging log, we have two implementation assumptions and one limitation.

9.1 Assumption 1
~~~~~~~~~~~~~~~~

From conf file, the determination factors for connection establish are **PrivateKey**, **Address**, **PublicKey** and **PresharedKey**. If one of these config value mismatch with server side, it causes connection failure. That means, if the PSK is mismatched, the VPN022 shown. Others, the VPN021 shown.

9.2 Assumption 2
~~~~~~~~~~~~~~~~

In all connection failure cases, the WireGuard will show infinite log, that means we need to implement some mechanism to suppress endless log. The possible solution is to record one log for all kind of failure connection druing a limited period. It implys only one log recorded before the timer expired even through user try to connect to server many times with the mismatch **PrivateKey**, **Address**, **PublicKey** and **PresharedKey**. 

9.3 Limitation
~~~~~~~~~~~~~~

For VPN022, VPN024 and VPN025, all need to wait about 540 seconds. We can log VPN022 after the timer expired by checking keypair created times. But, We **CAN NOT** distinguish the difference from VPN024 and VPN025. Even through the user tear down the connection gently, the server will not do the house keeping immediately. It still pretends the client will come back until the timeout time reach(540 seconds). So, VPN024 or VPN025 for connection lost??
